#!/usr/bin/env python
# -*- coding: utf-8 -*-

import scapy.all as scapy
import argparse


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--target", dest="target", help="Target IP / IP range.")
    (options) = parser.parse_args()
    return options


def scan(ip):
    # Set Address Resolution Protocol & ip destination
    arp_request = scapy.ARP(pdst=ip)
    # Set & Send to broadcast mac adress
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast / arp_request
    #  srp return 2 list : answered & unanswered  | timeout = don't wait
    #  answered = 0 | unanswered = 1 ; verbose=False to undisplay some information (nbre packet, ect)
    answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]
    # Print only answered response

    clients_list = []
    for element in answered_list:
        # For each element in answer : create a dictionnary
        client_dict = {"ip": element[1].psrc, "mac": element[1].hwsrc}
        #  Add each dictionnary as an element in client list
        clients_list.append(client_dict)
        # see the content with print(element[1].show()) then you can print only psrc, hwsrc, etc
        # see the ip adress (.psrc) of the response source & the mac adress (.hwsrc)
    return clients_list


def print_result(results_list):
    #  Print a header in result with IP & MAC
    print("IP\t\t\tMAC Adress\n-----------------------------------------")
    for client in results_list:
        print(client["ip"] + "\t\t" + client["mac"])


# Capture the value send with arguments
options = get_arguments()
# Assign target IP to the Scan
scan_result = scan(options.target)
# Run programme
print_result(scan_result)

